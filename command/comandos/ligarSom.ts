import {Comando} from './command';
import {Som} from '../aparelhos/som';

export class LigarSomCommand implements Comando {
    private som: Som;
    private data: number;

    constructor(som: Som, data: number) {
        this.som = som;
        this.data = data;
    }

    executar(): void {
        this.som.ligarSom(this.data);
    }
}