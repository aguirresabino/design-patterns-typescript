export class Som {

    public ligarSom(data: number): void {
        console.log(`O som será ligado em ${data}`);
    }

    public desligarSom(data: number): void {
        console.log(`O som será desligado em ${data}`);
    }
}