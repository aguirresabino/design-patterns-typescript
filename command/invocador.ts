import {Comando} from './comandos/command';
import {DesligarSomCommand} from './comandos/desligarSom'
import {LigarSomCommand} from './comandos/ligarSom'
import {Som} from './aparelhos/som';

export class Invocador {    

    public ligarSom(som:Som, data:number){
        let comando = new LigarSomCommand(som, data);
        comando.executar();
    }

    public desligarSom(som:Som, data:number){
        let comando = new DesligarSomCommand(som, data);
        comando.executar();
    }
}

let invocador:Invocador = new Invocador();

let som = new Som(); //inferindo o tipo

invocador.ligarSom(som, Date.now());
invocador.desligarSom(som, Date.now());